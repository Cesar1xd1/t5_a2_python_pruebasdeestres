'''
Created on 29 nov. 2020

@author: Cesar
'''
from time import time
import random

class Corrector:
    def correcion(self):
        e=1
        while e==1:
            try:
                r = int(input())
            except:
                print("Ups, solo numero enteros mayores a 0")
                e=1
            else:
                if r>0:
                    e=0
                else:
                    print("Ups, solo numeros entero mayores a 0")
                    e=1
        return r

class Burbuja:
    def ordenacionBurbuja1(self, numeros):
        tInicio = time()
        comparaciones =0
        intercambios = 0
        pasadas = 0
        pasadas+=1
        for i in range(1,len(numeros)):
            pasadas+=1
            for j in range(0,len(numeros)-i-1):
                comparaciones+=1
                if(numeros[j+1]<numeros[j]):
                    comparaciones = comparaciones +1
                    aux = numeros[j]
                    numeros[j] = numeros[j+1]
                    numeros[j+1]=aux
                    intercambios = intercambios +1
        tiempo = time()-tInicio
        print(f"Tiempo de ejecucion =  {tiempo}")
        print(f"Numerode intercambios = {intercambios}")
        print(f"Numero de comparaciones = {comparaciones}")
        print(f"Numero de pasadas = {pasadas}")
        

    def ordenacionBurbuja2(self, numeros):
        tInicio = time()
        comparaciones =0
        intercambios = 0
        pasadas = 0

        i = 1
        o = False
        pasadas+=1
        while(i<len(numeros)):
            i = i +1
            o = True
            pasadas+=1
            for j in range(0,len(numeros)-i-1):
                o = False
                comparaciones+=1
                intercambios+=1
                aux = numeros[j]
                numeros[j]=numeros[j+1]
                numeros[j+1]=aux
        tiempo = time()-tInicio
        print(f"Tiempo de ejecucion =  {tiempo}")
        print(f"Numerode intercambios = {intercambios}")
        print(f"Numero de comparaciones = {comparaciones}")
        print(f"Numero de pasadas = {pasadas}")
    
    def ordenacionBurbuja3(self, numeros):
        i = 1
        comparaciones =0
        intercambios = 0
        pasadas = 0
        tInicio = time()
        pasadas+=1
        while(i<len(numeros)):
            i = i+1
            o = True
            pasadas+=1
            for j in range(0,len(numeros)-i):
                comparaciones+=1
                if(numeros[j]>numeros[j+1]):
                    o = False
                    aux = numeros[j]
                    numeros[j]=numeros[j+1]
                    numeros[j+1]=aux
                    intercambios+=1
        tiempo = time()-tInicio
        print(f"Tiempo de ejecucion =  {tiempo}")
        print(f"Numerode intercambios = {intercambios}")
        print(f"Numero de comparaciones = {comparaciones}")
        print(f"Numero de pasadas = {pasadas}")           

class Seleccion:
    def ordenamienotSeleccion(self,numeros):
        comparaciones =0
        intercambios = 0
        pasadas = 0
        tInicio = time()
        pasadas+=1
        for i in range(len(numeros)):
            pasadas+=1
            
            for j in range(i,len(numeros)):
                comparaciones+=1
                if(numeros[i]>numeros[j]):
                    minimo = numeros[i]
                    numeros[i] = numeros[j]
                    numeros[j]=minimo
                    intercambios+=1
        tiempo = time()-tInicio
        print(f"Tiempo de ejecucion =  {tiempo}")
        print(f"Numerode intercambios = {intercambios}")
        print(f"Numero de comparaciones = {comparaciones}")
        print(f"Numero de pasadas = {pasadas}")           

class Insercion:
    def ordenarInsercion(self,numeros):
       
        aux = 0  
        comparaciones =0
        intercambios = 0
        pasadas = 0
        
        tInicio = time()
        pasadas+=1
        for i in range(1,len(numeros)):
            aux = numeros[i]
            j = (i-1)
            pasadas+=1
            comparaciones+=1
            while(j>=0 and numeros[j]>aux):
                numeros[j+1]=numeros[j]
                numeros[j]=aux
                j-=1
                comparaciones+=1
                intercambios+=1
        tiempo = time()-tInicio
        print(f"Tiempo de ejecucion =  {tiempo}")
        print(f"Numerode intercambios = {intercambios}")
        print(f"Numero de comparaciones = {comparaciones}")
        print(f"Numero de pasadas = {pasadas}")           

class QuickSort:
    comparaciones =0
    intercambios = 0
    pasadas = 0
    def quicksort(self,numeros,izq,der):
        pivote = numeros[izq]
        i = izq
        j = der
        aux = 0
        
        self.pasadas+=1
        while(i<j):
            self.pasadas+=1
            self.comparaciones+=1
            while(numeros[i]<=pivote and i <j):
                i = i +1
                self.pasadas+=1
                self.comparaciones+=1
            while(pivote < numeros[j]):
                j = j-1
            if(i<j):
                self.intercambios+=1
                aux = numeros[i]
                numeros[i] = numeros[j]
                numeros[j] = aux
        numeros[izq] = numeros[j]
        numeros[j] = pivote
        self.intercambios+=1
        if(izq < j-1):
            self.quicksort(numeros, izq, j-1)
            self.comparaciones+=1
        if(j+1<der):
            self.quicksort(numeros, j+1, der)
        
    def mostrarQuicksort(self,numeros,izq,der):
        tInicio=time()
        QuickSort.quicksort(self, numeros, izq, der)
        tiempo = time()-tInicio
        print(f"Tiempo de ejecucion =  {tiempo}")
        print(f"Numerode intercambios = {self.intercambios}")
        print(f"Numero de comparaciones = {self.comparaciones}")
        print(f"Numero de pasadas = {self.pasadas}")           

        
class Shellsort:
    
    def shellsort(self,numeros):
        comparaciones =0
        intercambios = 0
        pasadas = 0
        tInicio = time()
        intervalo = len(numeros)/2
        intervalo = int(intervalo)
        pasadas+=1
        while(intervalo>0):
            pasadas+=1
            for i in range(int(intervalo),len(numeros)):
                j = i-int(intervalo)
                pasadas+=1
                while(j>=0):
                    k = j + int(intervalo)
                    comparaciones+=1
                    if(numeros[j]<=numeros[k]):
                        j -=1
                    else:
                        aux = numeros[j]
                        intercambios+=1
                        numeros[j] = numeros[k]
                        numeros[k] = aux
                        j-=int(intervalo)
                    pasadas+=1
            intervalo = int(intervalo)/2
        tiempo = time()-tInicio
        print(f"Tiempo de ejecucion =  {tiempo}")
        print(f"Numerode intercambios = {intercambios}")
        print(f"Numero de comparaciones = {comparaciones}")
        print(f"Numero de pasadas = {pasadas}")
class RadixSort:
    comparaciones =0
    intercambios = 0
    pasadas = 0
    def radixOrder(self, numeros,exp1):
            
        
        n = len(numeros)
        cantidadDatos = [0] * (n)
        contar = [0]*(10)
        self.pasadas+=1
        for i in range(0,n):
            self.pasadas+=1
            indice = (numeros[i]/exp1)
            contar[int((indice)%10)]+=1
        for i in range(1,10):
            self.intercambios+=1
            contar[i]+=contar[i-1]
        i = n-1
        self.pasadas+=1
        while(i>=0):
            self.intercambios+=1
            indice = (numeros[i]/exp1)
            cantidadDatos[contar[int((indice)%10)]-1]=numeros[i]
            contar[int((indice)%10)]-=1
            i -=1
        i = 0
        for i in range(0,len(numeros)):
            numeros[i]=cantidadDatos[i]
    
    def radix(self,numeros):
        max1 = max(numeros)
        exp = 1
        tInicio = time()
        while(max1/exp)>0:
            RadixSort.radixOrder(self, numeros, exp)
            exp *= 10
        tiempo = time() - tInicio
        print(f"Tiempo de ejecucion =  {tiempo}")
        print(f"Numerode intercambios = {self.intercambios}")
        print(f"Numero de comparaciones = {self.comparaciones}")
        print(f"Numero de pasadas = {self.pasadas}")  
        
c = Corrector()
s = Seleccion()
ins = Insercion()
q = QuickSort()
b = Burbuja()
ss = Shellsort()
r = RadixSort()

opcion = 0
ooo =0
while(ooo!=1 and ooo!=2 and ooo!=3):
    print("Digite 1 para usar un vector con 1000 datos")
    print("Digite 2 para usar un vector con 10000 datos")
    print("Digite 3 para usar un vector con 100000 datos")
    ooo=c.correcion()


numeros = []
if(ooo==1):
    for i in range(1000):
        numeros.append(random.randint(0,1000))
elif(ooo==2):
    for i in range(10000):
        numeros.append(random.randint(0, 1000))
elif(ooo==3):
    for i in range(100000):
        numeros.append(random.randint(0, 1000))


while(opcion != 7):
    numeros2 = numeros.copy()
    print("======================== MENU =========================")
    print("Digite 1 pasa usar el metodo de ordenamiento BURBUJA")
    print("Digite 2 para usar el metodo de ordenamiento INSERCION")
    print("Digite 3 para usar el metodo de ordenamiento SELECCION")
    print("Digite 4 para usar el metodo de ordenamiento QUICKSORT")
    print("Digite 5 para usar el metodo de ordenamiento SHELLSORT")
    print("Digite 6 para usar le metodo de ordenamiento RADIXSORT")
    print("Digite 7 para ***SALIR***")
    opcion = c.correcion()
    
    if(opcion == 1):
        print("Digite 1 para usar el metodo de Burbuja 1")
        print("Digite 2 para usar el metodo de Burbuja 2")
        print("Digite 3 para usar el metodo de Burbuja 3")
        op2 = c.correcion()
        if(op2==1):
            print("=====Metodo de la burbuja #1=====")
            print(f"Numeros desordenados: {numeros2}" )
            b.ordenacionBurbuja1(numeros2)
            print(f"Numeros ordenados: {numeros2}")
        elif (op2==2):
            print("=====Metodo de la burbuja #2=====")
            print(f"Numeros desordenados: {numeros2}" )
            b.ordenacionBurbuja2(numeros2)
            print(f"Numeros ordenados: {numeros2}")
        elif(op2==3):
            print("=====Metodo de la burbuja #3=====")
            print(f"Numeros desordenados: {numeros2}" )
            b.ordenacionBurbuja3(numeros2)
            print(f"Numeros ordenados: {numeros2}")
        else:
            print("Opcion no disponinle")
    elif(opcion == 2):
        print("===== Metodo de Insercion =====")
        print(f"Numeros desordenados: {numeros2}")
        ins.ordenarInsercion(numeros2)
        print(f"Numeros desordenados: {numeros2}")
    elif(opcion==3):
        print("===== Metodo de Seleccion =====")
        print(f"Numeros Sin Ordenar: {numeros2}")
        s.ordenamienotSeleccion(numeros2)
        print(f"Numeros Ordenados: {numeros2}")
    elif(opcion==4):
        print("===== Metodo de Quicksort =====")
        print(f"Desordenados: {numeros2}")
        q.mostrarQuicksort(numeros2, 0,len(numeros2)-1)
        print(f"Ordenados: {numeros2}")
    elif(opcion == 7):
        print("Gracias por usar el programa")
    elif(opcion == 5):
        print("===== Metodo de Shellsort =====")
        print(f"Desordenados: {numeros2}")
        ss.shellsort(numeros2)
        print(f"Ordenados: {numeros2}")
    elif(opcion == 6):
        print("===== Metodo de RadixSort =====")
        print(f"Desordenados: {numeros2}")
        r.radix(numeros2)
        print(f"Ordenados: {numeros2}")
    else:
        print("Opcion no disponible")
